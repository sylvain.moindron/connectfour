@file:Suppress("OPT_IN_USAGE")

plugins {
    id("org.jetbrains.compose") apply false
    kotlin("multiplatform") apply false
}

group = "net.moindron"
version = "1.0-SNAPSHOT"
allprojects {
    repositories {
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
        maven("https://maven.pkg.jetbrains.space/kotlin/p/wasm/experimental")
        mavenCentral()

    }
}