import androidx.compose.material.MaterialTheme
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.WindowState
import androidx.compose.ui.window.application
import ui.gameApp


fun main() = application {
    val icon = painterResource("connect.png")
    Window(
        onCloseRequest = ::exitApplication,
        title = "Puissance 4",
        state = WindowState(),
        icon = icon,

        ) {
        MaterialTheme {
            gameApp()
        }

    }

}


