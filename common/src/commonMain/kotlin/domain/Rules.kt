package domain

enum class Player(val label: String) {
    YELLOW("jaune"),
    RED("rouge");

    fun nextPlayer(): Player = if (this == YELLOW) {
        RED
    } else {
        YELLOW
    }

}

data class Move(
    val player: Player,
    val column: Int,
    val row: Int,
)

fun List<Move>.playableRow(columnNumber: Int): Int? {
    val minimum =
        this.filter { it.column == columnNumber }.minByOrNull { it.row }?.row ?: return NUMBER_OF_ROWS

    if (minimum <= 1) {
        return null
    }
    return minimum - 1
}

fun List<Move>.availableMoves(player: Player) =
    (1..NUMBER_OF_COLUMNS)
        .mapNotNull { col ->
            this.playableRow(col)?.let { row -> Move(player, col, row) }
        }


fun List<Move>.isWinningMove(move: Move): Boolean {
    val myMoves = this.filter { it.player == move.player }

    // vertical win
    if (
        myMoves.contains(move.copy(row = move.row + 1)) &&
        myMoves.contains(move.copy(row = move.row + 2)) &&
        myMoves.contains(move.copy(row = move.row + 3))
    ) {
        return true
    }

    // horizontalWin
    var joinTokenNumber = 0
    if (myMoves.contains(move.copy(column = move.column + 1))) {
        joinTokenNumber++
        if (myMoves.contains(move.copy(column = move.column + 2))) {
            joinTokenNumber++
            if (myMoves.contains(move.copy(column = move.column + 3))) {
                joinTokenNumber++
            }
        }
    }
    if (myMoves.contains(move.copy(column = move.column - 1))) {
        joinTokenNumber++
        if (myMoves.contains(move.copy(column = move.column - 2))) {
            joinTokenNumber++
            if (myMoves.contains(move.copy(column = move.column - 3))) {
                joinTokenNumber++
            }
        }
    }

    if (joinTokenNumber >= 3) {
        return true
    }


    // diagonal slash win
    joinTokenNumber = 0
    if (myMoves.contains(move.copy(column = move.column + 1, row = move.row - 1))) {
        joinTokenNumber++
        if (myMoves.contains(move.copy(column = move.column + 2, row = move.row - 2))) {
            joinTokenNumber++
            if (myMoves.contains(move.copy(column = move.column + 3, row = move.row - 3))) {
                joinTokenNumber++
            }
        }
    }
    if (myMoves.contains(move.copy(column = move.column - 1, row = move.row + 1))) {
        joinTokenNumber++
        if (myMoves.contains(move.copy(column = move.column - 2, row = move.row + 2))) {
            joinTokenNumber++
            if (myMoves.contains(move.copy(column = move.column - 3, row = move.row + 3))) {
                joinTokenNumber++
            }
        }
    }

    if (joinTokenNumber >= 3) {
        return true
    }

    // diagonal backslash win
    joinTokenNumber = 0
    if (myMoves.contains(move.copy(column = move.column + 1, row = move.row + 1))) {
        joinTokenNumber++
        if (myMoves.contains(move.copy(column = move.column + 2, row = move.row + 2))) {
            joinTokenNumber++
            if (myMoves.contains(move.copy(column = move.column + 3, row = move.row + 3))) {
                joinTokenNumber++
            }
        }
    }
    if (myMoves.contains(move.copy(column = move.column - 1, row = move.row - 1))) {
        joinTokenNumber++
        if (myMoves.contains(move.copy(column = move.column - 2, row = move.row - 2))) {
            joinTokenNumber++
            if (myMoves.contains(move.copy(column = move.column - 3, row = move.row - 3))) {
                joinTokenNumber++
            }
        }
    }

    return joinTokenNumber >= 3
}

