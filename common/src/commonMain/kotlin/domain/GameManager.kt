package domain


import ai.ArtificialIntelligence
import ai.RandomPlayer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime


const val NUMBER_OF_ROWS = 6
const val NUMBER_OF_COLUMNS = 7

data class GameState(

    val moves: List<Move> = emptyList(),
    val curentPlayer: Player = Player.YELLOW,

    val winner: Player? = null,
    val computing: Boolean = false,
    val computers: Map<Player, ArtificialIntelligence?> = mapOf(
        Player.YELLOW to null,
        Player.RED to RandomPlayer
    ),


    ) {
    val ended = (winner != null) || (moves.size == (NUMBER_OF_ROWS * NUMBER_OF_COLUMNS))
}

class GameManager(
    private val coroutineScope: CoroutineScope,
) {

    private val _gameState = MutableStateFlow(GameState())
    val gameState = _gameState.asStateFlow()

    private var computeJob: Job? = null


    fun humanAddAToken(columnNumber: Int) {
        val playableRow = _gameState.value.moves.playableRow(columnNumber)
        if (playableRow != null) {
            val move = Move(_gameState.value.curentPlayer, columnNumber, playableRow)
            playMove(move)
        }
    }


    private fun playMove(move: Move) {

        val isWinningMove = _gameState.value.moves.isWinningMove(move)
        val nextPlayer = _gameState.value.curentPlayer.nextPlayer()

        _gameState.update {
            it.copy(
                moves = it.moves + move,
                curentPlayer = nextPlayer,
                winner = if (isWinningMove) {
                    it.curentPlayer
                } else {
                    null
                },
            )
        }

        startComputeurIfNeeded()


    }

    @OptIn(ExperimentalTime::class)
    private fun startComputeurIfNeeded() {

        val ia = _gameState.value.computers[_gameState.value.curentPlayer]

        if (ia == null || _gameState.value.ended) {
            return
        }

        computeJob = coroutineScope.launch {
            _gameState.update { it.copy(computing = true) }
            val nextMove: Move
            val duration = measureTime {
                nextMove = ia.computeNextMove(_gameState.value.moves, _gameState.value.curentPlayer)
            }.inWholeMilliseconds


            if (duration < MINIMUM_COMPUTE_TIME) {
                delay(MINIMUM_COMPUTE_TIME - duration)
            }

            playMove(nextMove)
            _gameState.update { it.copy(computing = false) }

        }
    }

    private fun stopComputing() {
        if (computeJob?.isActive == true) {
            computeJob?.cancel()
        }
        computeJob = null
        _gameState.update { it.copy(computing = false) }
    }


    fun reset() {
        stopComputing()

        _gameState.update {
            GameState(computers = it.computers)
        }
        startComputeurIfNeeded()
    }

    fun changePlayerAi(player: Player, ai: ArtificialIntelligence?) {
        _gameState.update {
            it.copy(computers = it.computers + Pair(player, ai))
        }

        //do we need to stop or start the comp ?
        if (player == _gameState.value.curentPlayer) {
            stopComputing()
            startComputeurIfNeeded()
        }

    }

    companion object {
        const val MINIMUM_COMPUTE_TIME = 500
    }


}
