package ui


import ai.aiOptions
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.RadioButton
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.unit.dp
import domain.*

@Composable
fun gameApp() {
    val coroutineScope = rememberCoroutineScope()
    val gameManager = remember { GameManager(coroutineScope) }

    Row {
        gameBoard(gameManager)
        informationPanel(gameManager)
    }
}


@Composable
fun gameBoard(gameManager: GameManager) {
    val gameManagerState by gameManager.gameState.collectAsState()
    Surface(color = Color.Blue) {
        Row {
            (1..NUMBER_OF_COLUMNS).forEach { columnNumber ->
                Column(
                    modifier = Modifier.clickable(
                        enabled = !gameManagerState.computing && !gameManagerState.ended && gameManagerState.moves.playableRow(
                            columnNumber
                        ) != null
                    ) {
                        gameManager.humanAddAToken(columnNumber)
                    }
                ) {
                    (1..NUMBER_OF_ROWS).forEach { rowNumber ->
                        val player =
                            gameManagerState.moves.find { it.column == columnNumber && it.row == rowNumber }?.player
                        playCase(player)
                    }
                }
            }
        }
    }
}

@Composable
fun playCase(player: Player?) {
    Surface(
        modifier = Modifier.padding(2.dp)
            .size(80.dp, 80.dp),
        color = Color.White
    ) {
        if (player != null) {
            Surface(
                shape = CircleShape,
                modifier = Modifier.padding(2.dp)
                    .size(80.dp, 80.dp),
                color = if (player == Player.RED) {
                    Color.Red
                } else {
                    Color.Yellow
                }
            ) {}
        }
    }
}

@Composable
fun informationPanel(gameManager: GameManager) {
    val gameManagerState by gameManager.gameState.collectAsState()
    Column(
        modifier = Modifier
            .padding(horizontal = 10.dp)
            .width(200.dp)
    ) {
        if (!gameManagerState.ended) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text("Joueur :")
                playCase(gameManagerState.curentPlayer)
            }
        }

        if (gameManagerState.ended) {
            Text(
                "partie terminée",
                modifier = Modifier.padding(vertical = 10.dp)
            )
        }
        if (gameManagerState.winner != null) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text("Vainqueur :")
                playCase(gameManagerState.winner)
            }
        }

        Button(
            onClick = { gameManager.reset() },
            modifier = Modifier.padding(vertical = 10.dp)
        ) {
            Text("Recommencer")
        }

        playerSelect(gameManager, Player.YELLOW)
        playerSelect(gameManager, Player.RED)

    }

}

@Composable
fun playerSelect(gameManager: GameManager, player: Player) {
    val radioOptions = aiOptions.entries.toList()
    val gameManagerState by gameManager.gameState.collectAsState()

    val selectedItem = gameManagerState.computers[player]

    Surface(
        border = BorderStroke(width = 1.dp, color = Color.Black),
        modifier = Modifier.padding(vertical = 3.dp)
    ) {
        Column {
            Text(text = "Joueur ${player.label} :")
            radioOptions.forEach { option ->
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .selectable(
                            selected = (selectedItem == option.value),
                            onClick = {
                                gameManager.changePlayerAi(player, option.value)
                            },
                            role = Role.RadioButton
                        )
                        .padding(horizontal = 16.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    RadioButton(
                        modifier = Modifier.padding(end = 16.dp),
                        selected = (selectedItem == option.value),
                        onClick = null // null recommended for accessibility with screen readers
                    )
                    Text(text = option.key)
                }
            }
        }
    }
}
