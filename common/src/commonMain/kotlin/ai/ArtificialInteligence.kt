package ai

import domain.Move
import domain.Player
import domain.availableMoves
import domain.isWinningMove

val aiOptions: Map<String, ArtificialIntelligence?> = mapOf(
    "Humain" to null,
    "Random" to RandomPlayer,
    "Simple" to SimplePlayer,
)

sealed interface ArtificialIntelligence {
    suspend fun computeNextMove(moves: List<Move>, player: Player): Move

}

object RandomPlayer : ArtificialIntelligence {
    override suspend fun computeNextMove(moves: List<Move>, player: Player): Move {

        // game is not finished there is a playable move to do !!
        return moves
            .availableMoves(player)
            .random()
    }
}

object SimplePlayer : ArtificialIntelligence {
    override suspend fun computeNextMove(moves: List<Move>, player: Player): Move {

        // game is not finished there is a playable move to do !!
        val availableMoves = moves.availableMoves(player)

        val aWinningMove = availableMoves.find { moves.isWinningMove(it) }
        if (aWinningMove != null) {
            return aWinningMove
        }

        val nonLoosingMoves = availableMoves.filter { !moves.isLoosingMove(it) }
        if (nonLoosingMoves.isNotEmpty()) {
            return nonLoosingMoves.random()
        }


        return availableMoves.random()

    }

    private fun List<Move>.isLoosingMove(move: Move): Boolean {
        val newMoves = this + move
        val availableAdverseMoves = newMoves.availableMoves(move.player.nextPlayer())
        return availableAdverseMoves.any { newMoves.isWinningMove(it) }

    }

}
